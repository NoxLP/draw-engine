const OBJECTS_TO_DRAW = [];
var canvas, ctx, player;

const clearAll = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
};

export const getCanvasMiddle = () => {
    return [canvas.width / 2, canvas.height / 2];
}

export const tickDraw = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //player.draw(ctx);

    for (let i = 0; i < OBJECTS_TO_DRAW.length; i++) {
        let object = OBJECTS_TO_DRAW[i];
        if(object.collision)
            object.drawCollision(ctx);
        else
            object.draw(ctx);
    }
}
export const loadCanvas = () => {
    console.log("loadCanvas")
    canvas = document.querySelector("canvas");
    ctx = canvas.getContext("2d");
};
export const addDrawableObject = object => {
    console.log("addDrawableObject", object.type, object)
    
    if(object.type === "player")
        player = object;
    
    OBJECTS_TO_DRAW.push(object);

    console.log("OBJECTS_TO_DRAW", JSON.stringify(OBJECTS_TO_DRAW, null))
};