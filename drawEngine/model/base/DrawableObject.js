var lastId = -1;

/**
 * Bottom inheritance object for objects that must be draw in canvas
 */
export class DrawableObject {
    constructor(positionInfo, type) {
        this._id = ++lastId;
        this._positionInfo = positionInfo;
        this._type = type;
        /**
         * Array of last direction(based on canvas axis) => [top/bottom, left/right] => 0 static, up <0, down >0, left <0, right >0
         */
        this.directionArray = [0, 0];
    }
    
    get positionInfo() { return this._positionInfo; }
    get type() { return this._type; }

    /**
     * Set new top left coordinates
     * @param {number} x New x coordinate
     * @param {number} y New y coordinate
     */
    setCoords(x, y) {
        this.setDirectionArray(x, y);
        //console.log("DIRECTION",this.directionArray)
        this._positionInfo.setCoords(x, y);
    }
    /**
     * Set new center coordinates
     * @param {number} x New x coordinate
     * @param {number} y New y coordinate
     */
    setCenterCoords(x, y) {
        x -= (this._positionInfo._width / 2);
        y -= (this._positionInfo._height / 2);

        this.setDirectionArray(x, y);
        //console.log("DIRECTION",this.directionArray)
        this._positionInfo.setCoords(x, y);
    }
    /**
     * Move top left object's coordinates by distances in distanceArray. Does NOT move the object in the canvas, just change the object's coordinates.
     * @param {Array} distanceArray Distances that the object must move in [x,y] axis.
     */
    move(distanceArray) {
        this.setDirectionArrayByDistance(distanceArray);
        //console.log("DIRECTION D", this.directionArray)
        this._positionInfo._x += distanceArray[0];
        this._positionInfo._y += distanceArray[1];
    }
    setDirectionArray(newX, newY) {
        this.directionArray = [
            newX - this._positionInfo.x,
            newY - this._positionInfo.y
        ];
    }
    setDirectionArrayByDistance(distanceArray) {
        this.directionArray = distanceArray;
    }
    /**
     * Override this method when inherit DrawableObject, it will be called at every tick to draw the object on the canvas.
     * @param {canvas context} ctx Canvas context
     */
    draw(ctx) { return undefined; }
    equals(object) {
        if(object.hasOwnProperty("_id") && object._id === this._id)
            return true;
        return false;
    }
}