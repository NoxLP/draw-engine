/**
 * Wrapper for box coordinates and size
 */
export class BoxPositionInfo {
    constructor(leftX, topY, width, height) {
        this._width = width;
        this._height = height;
        this._x = leftX;
        this._y = topY;
    }
    get width() { return this._width; }
    get height() { return this._height; }
    get x() {return this._x; }
    get y() {return this._y; }
    get topLeftCoords () { return [this._x, this._y] };
    get topRightCoords () { return [this._x + this._width, this._y] };
    get centerCoords () { return [this._x + (this._width / 2), this._y + (this._height / 2)] };
    get bottomLeftCoords () { return [this._x, this._y + this._height] };
    get bottomRightCoords () { return [this._x + this._width, this._y + this._height] };
    setCoords(x, y) {
        this._x = x;
        this._y = y;
    }
}