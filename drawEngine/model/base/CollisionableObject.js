import { DrawableObject } from "./DrawableObject.js"

/**
 * Objects that have collisions have to inherit from this one
 */
export class CollisionableObject extends DrawableObject {
    constructor (positionInfo, type) {
        super(positionInfo, type);
        this.collision = false;
        this.collisionCallback = undefined;
    }
    /**
     * Check if this collides with other. Does NOT check if other is a ColisionableObject.
     * @param {CollisionableObject} other ColisionableObject to check if it collide with this. Does NOT check if movingObject is a ColisionableObject.
     * @todo Esto tendría que diferenciar entre tipos de objetos, por ejemplo dos disparos no deberían colisionar entre sí. ¿White/blacklist?
     */
    collideWith(other) {
        if (this.positionInfo.topLeftCoords[0] < other.positionInfo.topLeftCoords[0] + other.positionInfo.width &&
            this.positionInfo.topLeftCoords[0] + this.positionInfo.width > other.positionInfo.topLeftCoords[0] &&
            this.positionInfo.topLeftCoords[1] < other.positionInfo.topLeftCoords[1] + other.positionInfo.height &&
            this.positionInfo.topLeftCoords[1] + this.positionInfo.height > other.positionInfo.topLeftCoords[1])
            return true;
        return false;
    }
    /**
     * Override this method when inherit CollisionableObject, it will be called by view when this collide with other object.
     * @param {canvas context} ctx Canvas context
     */
    drawCollision(ctx) { return undefined; }
}
/* *********************************  OLD
collideWithByDirection
//derecha
        if((directionArray[0] > 0 && collObject.positionInfo.topRight[0] >= this.positionInfo.topLeft[0]) &&
           (directionArray[1] === 0 ||                                                                              //SOLO derecha
           (directionArray[1] > 0 && collObject.positionInfo.bottomRight[1] >= this.positionInfo.topLeft[1]) ||     //oblicuo derecha abajo
           (directionArray[1] < 0 && collObject.positionInfo.topRight[1] <= this.positionInfo.bottomLeft[1]))) {    //oblicuo derecha arriba
            return true;
        }
        //izquierda
        else if((directionArray[0] < 0 && collObject.positionInfo.topLeft[0] <= this.positionInfo.topRight[0]) &&
           (directionArray[1] === 0 ||                                                                              //SOLO izquierda
           (directionArray[1] > 0 && collObject.positionInfo.bottomLeft[1] >= this.positionInfo.topRight[1]) ||     //oblicuo izq abajo
           (directionArray[1] < 0 && collObject.positionInfo.topLeft[1] <= this.positionInfo.bottomRight[1]))) {    //oblicuo izq arriba
            return true;
        }
        //estático en eje x
        else if(directionArray[0] === 0 && (
           (directionArray[1] > 0 && collObject.positionInfo.bottomLeft[1] >= this.positionInfo.topLeft[1]) ||      //bajando
           (directionArray[1] < 0 && collObject.positionInfo.topLeft[1] <= this.positionInfo.bottomLeft[1]))) {     //subiendo
            return true;
        }

        return false;
*/