import { Player } from "./Player.js"
import { BoxPositionInfo } from "./base/BoxPositionInfo.js"
import { TestCollsBorrameObject } from "./TestCollsBorrameObject.js";
import { CollisionableObject } from "./base/CollisionableObject.js";

export const OBJECTS_ALL = [], OBJECTS_BY_TYPE = {};

export var player;

const storeObject = object => {
    OBJECTS_ALL.push(object);

    if(OBJECTS_BY_TYPE.hasOwnProperty(object.type)) {
        OBJECTS_BY_TYPE[object.type].push(object);
    } else {
        OBJECTS_BY_TYPE[object.type] = [object];
    }
}
const absDistance = (coords1, coords2) => {
    return Math.abs(Math.sqrt(
        ((coords1[0] - coords2[0]) ** 2) +
        ((coords1[1] - coords2[1]) ** 2)));
}

/**
 * Find objects that are inside threshold distance of object AND aren't which type is not typeNot
 * @param {DrawableObject} object Object to find other objects near to it
 * @param {number} threshold Maximum distance of other objects
 * @param {string} typeNot Objects of this type won't be included in the search
 * @param {boolean} onlyCollisionable Only objects instance of CollisionableObject s will be included in the search
 * @todo Esto hay que probarlo con objetos que no sean CollisionableObject
 */
export const getObjectsNearObject = (object, threshold, typeNot, onlyCollisionable) => {
    let types = Object.keys(OBJECTS_BY_TYPE).filter(x => (!x || x === "" || x !== typeNot) && (!onlyCollisionable || x instanceof CollisionableObject));
    console.log("TYPES", types)
    let result = [];
    types.forEach(x => { result = result.concat(OBJECTS_BY_TYPE[x].filter(y => {
        console.log(y)
        console.log(!object.equals(y), absDistance(object.positionInfo.topLeftCoords, y.positionInfo.topLeftCoords))
        return !object.equals(y) && absDistance(object.positionInfo.topLeftCoords, y.positionInfo.topLeftCoords) <= threshold
    })); });
    return result;
}
export const createPlayer = (x, y) => {
    console.log("Create player at ", x,y)
    player = new Player(new BoxPositionInfo(x,y,50,50));
    player.setCenterCoords(x,y);
    storeObject(player);
    console.log(player.positionInfo.centerCoords)
    console.log(player);
    return player;
}

/****************** collisions test - deleteme ****************************/
export const createCollTestObject = () => {
    let obj = new TestCollsBorrameObject(new BoxPositionInfo(3,3,30,30));
    storeObject(obj);
    return obj;
}    
/**************************************************************************/