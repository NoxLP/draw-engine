import { CollisionableObject } from "./base/CollisionableObject.js";

export class TestCollsBorrameObject extends CollisionableObject {
    constructor (positionInfo) {
        super(positionInfo, "TestCollsBorrameObject");
    }
    draw(ctx) {
        ctx.fillStyle = "blue";
        ctx.fillRect(this.positionInfo._x,this.positionInfo._y,this.positionInfo._width,this.positionInfo._height);
    }
    drawCollision(ctx) {
        ctx.fillStyle = "red";
        ctx.fillRect(this.positionInfo._x,this.positionInfo._y,this.positionInfo._width,this.positionInfo._height);
    }
}