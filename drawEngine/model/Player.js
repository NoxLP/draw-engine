import { CollisionableObject } from "./base/CollisionableObject.js";

export class Player extends CollisionableObject{
    constructor (positionInfo) {
        super(positionInfo,"player");
    }
    draw(ctx) {
        ctx.fillStyle = "white";
        ctx.fillRect(this.positionInfo._x,this.positionInfo._y,this.positionInfo._width,this.positionInfo._height);
    }
    drawCollision(ctx) {
        console.log("************* PLAYER COLLISION at", this.positionInfo.topLeftCoords);
        ctx.fillStyle = "green";
        ctx.fillRect(this.positionInfo._x,this.positionInfo._y,this.positionInfo._width,this.positionInfo._height);
    }
}