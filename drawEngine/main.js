import * as Controller from "./controller/controller.js";



window.onload = () => {
    let startB = document.getElementById("startB");
    startB.onclick = Controller.startGame;
    let stopB = document.getElementById("stopB");
    stopB.onclick = Controller.stopGame;
};