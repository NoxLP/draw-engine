import * as View from "../view/view.js";
import * as Model from "../model/model.js";
import * as Input from "./inputController.js";
import * as MovementController from "./movementController.js"

const FPS = 15;// ~= 1/60
//var intervalID;
var gameRunning = null;

export const startGame = (createPlayer = true) => {
    console.log("****** GAME STARTED ******");

    View.loadCanvas();
    //intervalID = window.setInterval(tick, FPS);
    gameRunning = true;
    window.requestAnimationFrame(tick);
    console.log(...View.getCanvasMiddle())
    
    if(!Model.player && createPlayer) {
        View.addDrawableObject(Model.createPlayer(...View.getCanvasMiddle()));
        //console.log("VIEW PLAYER", View.player);
    }

    /****************** collisions test - deleteme ****************************/
    View.addDrawableObject(Model.createCollTestObject());
    /**************************************************************************/
}
export const stopGame = () => {
    console.log("****** GAME STOPPED ******");
    //clearInterval(intervalID);
    gameRunning = false;
}


const tick = () => {
    if(!gameRunning)
        return;

    MovementController.moveObjects();
    View.tickDraw();
    setTimeout(() => window.requestAnimationFrame(tick), FPS);
}