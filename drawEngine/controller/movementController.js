import * as Model from "../model/model.js";
import * as Input from "./inputController.js";

//********************* OJO TODO ************************/
// ESTO ES PARA TESTEAR. CADA OBJETO DEBERIA TENER SU PROPIA VELOCIDAD.
// HAY QUE CAMBIARLO
const MOVE_SPEED = 0.5;
//********************* OJO ************************/
var movingObjects = [];

const getPlayerMoveAmount = () => {
    let xMove = 0, yMove = 0;
    Input.KEYS_PRESSED.forEach(x => {
        switch(x) {
            case "ArrowUp":
                yMove -= MOVE_SPEED;
                break;
            case "ArrowRight":
                xMove += MOVE_SPEED;
                break;
            case "ArrowDown":
                yMove += MOVE_SPEED;
                break;
            case "ArrowLeft":
                xMove -= MOVE_SPEED;
                break;
        }
    });
    return [xMove, yMove];
}

const calcNearObjectThreshold = object => {
    return 50 + ((object.positionInfo.centerCoords.reduce((x,acc) => x + acc, 0)) / 2);
}
const checkCollisions = () => {
    let nearObjects = Model.getObjectsNearObject(Model.player, calcNearObjectThreshold(Model.player), "player");
    console.log("NEAR OBJECTS ", nearObjects)
    //console.log(`-------- Checking collisions. Near objects to player: `, nearObjects);
    nearObjects.forEach(x => {
        Model.player.collision = Model.player.collideWith(x);
        x.collision = Model.player.collision;
        console.log("COLLISION",Model.player.collision)
    });

    //TODO: esto tiene que iterar por todos los movingObjects menos el player, viendo si hay alguna colisión
}
export const moveObjects = () => {
    //TODO: mueve el resto de objetos
    
    if(Input.KEYS_PRESSED.length !== 0) {
        movingObjects.unshift(Model.player);
        let playerMove = getPlayerMoveAmount();
        Model.player.move(playerMove);
    }

    if(movingObjects.length > 0)
        checkCollisions();
    
    movingObjects = [];
}