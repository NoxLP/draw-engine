export const KEYS_PRESSED = [];
const MOVE_KEYS = ["ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"];

document.addEventListener("keydown", e => {
    //https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
    //Internet Explorer, Edge (16 and earlier), and Firefox (36 and earlier) use "Left", "Right", "Up", and "Down" instead of "ArrowLeft", "ArrowRight", "ArrowUp", and "ArrowDown".
    //console.log("KEYDOWN");
    if(!MOVE_KEYS.includes(e.key))
        return;

    let index;
    if((index = KEYS_PRESSED.indexOf(e.key)) == -1) {
        KEYS_PRESSED.push(e.key);
    }
});
document.addEventListener("keyup", e => {
    //https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
    //Internet Explorer, Edge (16 and earlier), and Firefox (36 and earlier) use "Left", "Right", "Up", and "Down" instead of "ArrowLeft", "ArrowRight", "ArrowUp", and "ArrowDown".
    //console.log("KEYUP");
    if(!MOVE_KEYS.includes(e.key))
        return;
        
    let index;
    if((index = KEYS_PRESSED.indexOf(e.key)) != -1) {
        KEYS_PRESSED.splice(index, 1);
    }    
});